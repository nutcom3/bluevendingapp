import React, { useRef, useEffect, useState, useCallback } from "react";
import { Container, Row, Col } from "react-bootstrap";
import styles from "./Home.module.css";

import InventoryComponent from "../../components/Inventory/InventoryComponent";
import WalletComponent from "../../components/Wallet/WalletComponent";

import { fetchPosts } from "../../api/vendingMachineApi";
import CheckBillComponent from "../../components/Checkbill/CheckBillComponent";

const HomePage = () => {
  const [list, setList] = useState([]);
  const [selectedItem, setSelectedItem] = useState();
  const targetRef = useRef(null);

  useEffect(() => {
    const load = async () => {
      try {
        const response = await fetchPosts();
        setList(response.data);
      } catch (err) {}
    };
    load();
  }, []);

  const scrollToComponent = useCallback(() => {
    if (selectedItem && targetRef.current) {
      targetRef.current.scrollIntoView({ behavior: "smooth" });
    }
  }, [selectedItem]);

  useEffect(() => {
    scrollToComponent();
  }, [selectedItem, scrollToComponent]);

  const handleItemClick = (newSelectItem) => {
    clearSelectedItem();
    setSelectedItem(newSelectItem);
  };

  const handleClearClick = () => {
    setSelectedItem();
  };

  const handleTrashClick = () => {
    clearSelectedItem();
    setSelectedItem(null);
  };

  const clearSelectedItem = () => {
    if (!selectedItem) return;

    const index = list.findIndex((item) => item.id === selectedItem.id);

    if (index !== -1) {
      const newList = [...list];
      newList[index].item_amount += 1;
      setList(newList);
    }
  };

  const renderRows = () => {
    const rows = [];

    for (let i = 0; i < 9; i += 3) {
      const itemsInRow = list.slice(i, i + 3).map((item, index) => (
        <Col key={index} xs="12" sm="4" lg="3" className="mb-4">
          <InventoryComponent item={item} onChildClick={handleItemClick} />
        </Col>
      ));
      rows.push(
        <Row key={i} className="justify-content-md-center p-2">
          {itemsInRow}
        </Row>
      );
    }
    return rows;
  };

  return (
    <>
      <Container>
        <div className="text-center pt-4">
          <h1>Blue Vending Machine</h1>
          <p>
            Snack, Interact, and Smile Back: Your Friendly Vending Experience
          </p>
        </div>
        <div className={`${styles.itemList}`}>{renderRows()}</div>

        {selectedItem && (
          <div ref={targetRef}>
            <CheckBillComponent
              item={selectedItem}
              onChildClick={handleTrashClick}
            />
            <WalletComponent item={selectedItem} onClear={handleClearClick} />
          </div>
        )}
      </Container>
    </>
  );
};

export default HomePage;
