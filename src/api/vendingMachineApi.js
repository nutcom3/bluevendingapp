const BASE_URL = process.env.REACT_APP_API_URL;

export const fetchPosts = async () => {
  try {
    const response = await fetch(`${BASE_URL}/vendingmachine/1`);
    if (!response.ok) {
      throw new Error("Network response was not ok");
    }
    return await response.json();
  } catch (error) {
    console.error("Fetching posts failed:", error);
    throw error;
  }
};

export const postData = async (data) => {
  try {
    const response = await fetch(`${BASE_URL}/posts`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(data),
    });

    if (!response.ok) {
      throw new Error("Network response was not ok");
    }

    return await response.json();
  } catch (error) {
    console.error("Error posting data:", error);
    throw error;
  }
};

export const checkOut = async (data) => {
  try {
    const response = await fetch(`${BASE_URL}/checkout`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(data),
    });

    const responseData = await response.json();

    if (!response.ok) {
      const errorMessage = responseData.error || "Network response was not ok";
      console.error("API error response:", errorMessage);
      throw new Error(errorMessage);
    }

    return responseData;
  } catch (error) {
    console.error("Error calling API:", error);
    throw error;
  }
};
