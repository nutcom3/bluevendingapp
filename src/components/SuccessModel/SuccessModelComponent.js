import React from "react";
import { Modal, Button } from "react-bootstrap";
import { CheckCircle } from 'react-bootstrap-icons';

function SuccessModelComponent({ show, handleClose, paidInfo }) {
  const nameMapping = {
    coin_1: "coin 1",
    coin_5: "coin 5",
    coin_10: "coin 10",
    banknote_20: "banknote 20",
    banknote_50: "banknote 50",
    banknote_100: "banknote 100",
    banknote_500: "banknote 500",
    banknote_1000: "banknote 1000",
  };
  return (
    <Modal show={show} onHide={handleClose}>
      <Modal.Header closeButton>
        <Modal.Title>
          Payment Successful ! <CheckCircle />
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <div>
          <strong>Total Price</strong>: {paidInfo?.total_price}
        </div>
        <div>
          <strong>Total Insert</strong>: {paidInfo?.total_inserted_amount}
        </div>
        <br />
        <strong>Change list</strong>:
        {paidInfo &&
        paidInfo.banknotes_change &&
        Object.keys(paidInfo.banknotes_change).length > 0 ? (
          Object.entries(paidInfo.banknotes_change).map(([coin, count]) => (
            <div key={coin}>
              {nameMapping[coin] || coin} : {count}
            </div>
          ))
        ) : (
          <div>No change</div>
        )}
      </Modal.Body>
      <Modal.Footer>
        <Button variant="secondary btn btn-sm" onClick={handleClose}>
          Close
        </Button>
      </Modal.Footer>
    </Modal>
  );
}

export default SuccessModelComponent;
