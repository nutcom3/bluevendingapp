import React, { useState, useEffect, useCallback } from "react";
import TotalbillComponent from "../Totalbill/TotalbillComponent";
import SuccessModelComponent from "../SuccessModel/SuccessModelComponent";
import ErrorModelComponent from "../ErrorModel/ErrorModelComponent";

import { checkOut } from "../../api/vendingMachineApi";

function WalletComponent({ item, onClear }) {
  const [showModal, setShowModal] = useState(false);
  const [error, setError] = useState(false);
  const [insertedAmount, setInsertedAmount] = useState(0.0);
  const [bankNotes] = useState({
    coin_1: 1,
    coin_5: 5,
    coin_10: 10,
    banknote_20: 20,
    banknote_50: 50,
    banknote_100: 100,
    banknote_500: 500,
    banknote_1000: 1000.0,
  });

  const [insertedCount, setInsertedCount] = useState({
    coin_1: 0,
    coin_5: 0,
    coin_10: 0,
    banknote_20: 0,
    banknote_50: 0,
    banknote_100: 0,
    banknote_500: 0,
    banknote_1000: 0,
  });

  const [paidInfo, setPaidInfo] = useState();

  const calculateMoney = useCallback(
    (insertedMoneys) => {
      let total = 0;
      for (let insertedMoney in insertedMoneys) {
        if (
          insertedMoneys.hasOwnProperty(insertedMoney) &&
          bankNotes.hasOwnProperty(insertedMoney)
        ) {
          total += bankNotes[insertedMoney] * insertedMoneys[insertedMoney];
        }
      }
      setInsertedAmount(total);
    },
    [bankNotes]
  );

  useEffect(() => {
    calculateMoney(insertedCount);
  }, [insertedCount, calculateMoney]);

  const createCheckoutObject = useCallback(() => {
    const checkoutObject = {
      total_price: item.value,
      total_inserted_amount: insertedAmount,
      vending_machine_id: 1,
      inventory_vending_machine_id: 1,
      coin_1: insertedCount.coin_1,
      coin_5: insertedCount.coin_5,
      coin_10: insertedCount.coin_10,
      banknote_20: insertedCount.banknote_20,
      banknote_50: insertedCount.banknote_50,
      banknote_100: insertedCount.banknote_100,
      banknote_500: insertedCount.banknote_500,
      banknote_1000: insertedCount.banknote_1000,
    };

    return checkoutObject;
  }, [
    item.value,
    insertedAmount,
    insertedCount.coin_1,
    insertedCount.coin_5,
    insertedCount.coin_10,
    insertedCount.banknote_20,
    insertedCount.banknote_50,
    insertedCount.banknote_100,
    insertedCount.banknote_500,
    insertedCount.banknote_1000,
  ]);

  useEffect(() => {
    if (insertedAmount >= item.value) {
      const checkoutObject = createCheckoutObject();
      checkOut(checkoutObject)
        .then((result) => {
          setPaidInfo(result);
          setShowModal(true);
        })
        .catch((error) => {
          setError(error);
        });
    }
  }, [insertedAmount, item.value, createCheckoutObject]);

  const handleClick = (key) => {
    setInsertedCount({
      ...insertedCount,
      [key]: insertedCount[key] + 1,
    });
  };

  const handleClose = () => {
    setShowModal(false);
    onClear();
    setInsertedAmount(0);
    setInsertedCount({
      coin_1: 0,
      coin_5: 0,
      coin_10: 0,
      banknote_20: 0,
      banknote_50: 0,
      banknote_100: 0,
      banknote_500: 0,
      banknote_1000: 0,
    });
  };

  return (
    <>
      <ErrorModelComponent error={error} handleClose={handleClose} />
      <SuccessModelComponent
        show={showModal}
        handleClose={handleClose}
        paidInfo={paidInfo}
      />
      <TotalbillComponent value={item.value} insertedAmount={insertedAmount} />
      <div className="text-center pt-5">
        <p>Please insert only</p>
        <div>
          {Object.entries(bankNotes).map(([key, banknote]) => {
            const borderRadiusStyle =
              banknote < 20
                ? { borderRadius: "50%", width: "35px", height: "35px" }
                : {};

            return (
              <button
                key={key}
                type="button"
                className="btn btn-outline-secondary btn-sm m-2"
                style={borderRadiusStyle}
                onClick={() => handleClick(key)}
              >
                {banknote}
              </button>
            );
          })}
        </div>
      </div>
    </>
  );
}

export default WalletComponent;
