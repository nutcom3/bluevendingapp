import React from "react";

function TotalbillComponent({ value, insertedAmount }) {
  return (
    <div className="row">
      <div className="pt-5 col-md-8 offset-md-2">
        <div className="row">
          <div className="col-6 col-sm-6"></div>
          <div className="col-4 col-sm-4">Balance:</div>
          <div className="col-2 col-sm-2" style={{ textAlign: "right" }}>
            <strong> {value}</strong>
          </div>
        </div>
        <div className="row">
          <div className="col-6 col-sm-6"></div>
          <div className="col-4 col-sm-4">Inserted amount:</div>
          <div className="col-2 col-sm-2" style={{ textAlign: "right" }}>
            <strong> {insertedAmount.toFixed(2)}</strong>
          </div>
        </div>
      </div>
    </div>
  );
}

export default TotalbillComponent;
