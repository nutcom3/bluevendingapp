import React from "react";
import defaultImage from "../../assets/default.png";
import { Trash } from "react-bootstrap-icons";

function CheckBillComponent(props) {
  const handleTrashClick = () => {
    props.onChildClick(props.item);
  };

  return (
    <>
      <div className="row">
        <div className="pt-5 col-md-8 offset-md-2">
          <div className="row">
            <div className="col-10 col-sm-11">
              <h5>Selected item:</h5>
            </div>
            <div className="col-2 col-sm-1">
              <Trash size={15} color="red" onClick={handleTrashClick} />
            </div>
          </div>
          <ul className="list-group">
            <li className="list-group-item">
              <div className="row">
                <div className="col-sm-3">
                  <img
                    alt="billprocess"
                    src={props.item?.image_url || defaultImage}
                    style={{ width: "100px", height: "100px" }}
                  />
                </div>
                <div className="col-sm-5">
                  <div>
                    {props.item.name}
                    <p style={{ color: "#A9A9A9" }}>{props.item.description}</p>
                  </div>
                </div>
                <div className="col-sm-2" style={{ textAlign: "right" }}>
                  Qty: 1
                </div>
                <div className="col-sm-2" style={{ textAlign: "right" }}>
                  Price: {props.item.value}
                </div>
              </div>
            </li>
          </ul>
        </div>
      </div>
    </>
  );
}

export default CheckBillComponent;
