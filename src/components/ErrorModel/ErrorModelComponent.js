import React from "react";
import { Modal, Button } from "react-bootstrap";
import { CheckCircle } from "react-bootstrap-icons";

function ErrorModelComponent({ error, handleClose }) {
  return (
    <>
      {error && (
        <Modal show={true} onHide={handleClose}>
          <Modal.Header closeButton>
            <Modal.Title>Payment Fail !</Modal.Title>
          </Modal.Header>
          <Modal.Body>{error.message}</Modal.Body>
          <Modal.Footer>
            <Button variant="danger btn btn-sm" onClick={handleClose}>
              Close
            </Button>
          </Modal.Footer>
        </Modal>
      )}
    </>
  );
}

export default ErrorModelComponent;
