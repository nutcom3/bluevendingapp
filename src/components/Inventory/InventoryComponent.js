import React from "react";
import Card from "react-bootstrap/Card";
import defaultImage from "../../assets/default.png";
import styles from "./Inventory.module.css";

function InventoryComponent({ onChildClick, item }) {
  const handleClick = () => {
    onChildClick(item);
    item.item_amount = Math.max(0, item?.item_amount - 1);
    window.scrollTo({
      top: document.documentElement.scrollHeight,
      behavior: "smooth",
    });
  };

  return (
    <Card>
      <Card.Img
        variant="top"
        src={item?.image_url || defaultImage}
        className={`${styles.itemImage}`}
      />
      <Card.Body
        className={`${styles.itemCard}`}
        style={{ position: "relative" }}
      >
        <div className="d-flex justify-content-center">
          <Card.Title>
            {item?.name} ({item?.item_amount})
          </Card.Title>
        </div>
        <Card.Text
          className="fs-6 d-flex justify-content-center"
          style={{ height: "50px" }}
        >
          {item.description}
        </Card.Text>
        <button
          type="button"
          onClick={handleClick}
          className="btn btn-outline-primary btn-sm"
          style={{
            position: "absolute",
            bottom: "10px",
            left: "50%",
            transform: "translateX(-50%)",
          }}
        >
          {item?.value}
        </button>
      </Card.Body>
    </Card>
  );
}

export default InventoryComponent;
