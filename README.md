# Blue Vending

    Support Node 16

## Development

1. run `npm install`

2. run `npm start`

## Deployment

1. run `docker build -t blue-vendingmachine-react-app . `

2. run `docker-compose up -d`